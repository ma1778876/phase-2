#imports
import json
import pickle
import re
import ast
import pandas as pd
import nltk
import numpy as np
from nltk.stem import WordNetLemmatizer
from nltk import word_tokenize
from nltk.corpus import stopwords
from sklearn.metrics import  accuracy_score
import string
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, confusion_matrix, roc_auc_score
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')
stop_words_ = set(stopwords.words('english'))
wn = WordNetLemmatizer()

from sklearn.base import BaseEstimator, TransformerMixin
class ItemSelector(BaseEstimator, TransformerMixin):
    def __init__(self, key):
        self.key = key

    def fit(self, x, y=None):
        return self

    def transform(self, data_dict):
        return data_dict[self.key]

def calculate_metrics(test_data, predictions):
    print("Accuracy Score:")
    print(f"{round(accuracy_score(test_data,predictions)*100,3)} %")

def read_data(input_path="data/structured-data"):
    res_1 = pd.read_csv(f"{input_path}/data_manual.csv")
    res_2 = pd.read_csv(f"{input_path}/data_scraper.csv")
    res_3 = pd.read_csv(f"{input_path}/data_fb.csv")
    res = res_2.append([res_1,res_3])
    return res

def black_txt(token):
    return  token not in stop_words_ and token not in list(string.punctuation) and len(token)>2

def clean_txt(text):
    clean_text = []
    clean_text2 = []
    text = re.sub("'", "",text)
    text=re.sub("(\\d|\\W)+"," ",text)    
    clean_text = [ wn.lemmatize(word, pos="n") for word in word_tokenize(text.lower()) if black_txt(word)]
    clean_text2 = [word for word in clean_text if black_txt(word)]
    return " ".join(clean_text2)

def transformation(res):
    res["text"] = res["text"].apply(lambda x : ast.literal_eval(x).decode("utf-8"))
    counts = res['category'].value_counts()
    model_df = res[~res['category'].isin(counts[counts < 2].index)]
    model_df.reset_index(inplace= True, drop=True)
    model_df["clean_text"] = model_df["text"].apply(lambda a : clean_txt(a))
    return model_df

def main():
    res = read_data()
    model_df = transformation(res)
    seed = 42
    encoder = LabelEncoder()
    
    X= model_df["clean_text"]
    y_unencoded = model_df["category"]
    y = encoder.fit_transform(y_unencoded)
    X_train, X_test, y_train, y_test = train_test_split(X, y_unencoded, test_size=0.15, random_state=seed, stratify=y)

    encoding_dict = {}
    for idx,a in enumerate(y):
        encoding_dict[int(a)]=y_unencoded[idx]
    with open("encoding.json", "w") as outfile:
        json.dump(encoding_dict, outfile)

    #Naive Bayes Pipeline
    naive_bayes_pipeline = Pipeline(
        [('tfidf', TfidfVectorizer()),
        ('clf', MultinomialNB()),
        ]
    )
    naive_bayes_pipeline.fit(X_train,y_train)
    nb_predicted = naive_bayes_pipeline.predict(X_test)
    
    #Stochastic Gradient Descent
    sgd_pipeline = Pipeline(
        [('tfidf', TfidfVectorizer()),
        ('clf', SGDClassifier(loss='modified_huber', penalty='l2',alpha=0.003,random_state=42,max_iter=10000)),
    ])
    sgd_pipeline.fit(X_train,y_train)
    sgd_predicted=sgd_pipeline.predict(X_test)
    
    #K-nearest neighbor
    knn_pipeline = Pipeline([
    ('tfidf', TfidfVectorizer()),
    ('clf', KNeighborsClassifier())])
    knn_pipeline.fit(X_train,y_train)
    knn_predicted=knn_pipeline.predict(X_test)
    
    #Support Vector Machine
    clf_sv = LinearSVC(C=1, class_weight='balanced', multi_class='ovr', random_state=42, max_iter=10000) #Support Vector machines
    sv_pipeline = Pipeline([
        ('tfidf', TfidfVectorizer()),
        ('clf', clf_sv)])
    sv_pipeline.fit(X_train,y_train)
    sv_predicted=sv_pipeline.predict(X_test)

    #Random Forests
    rfc_pipeline = Pipeline([
    ('tfidf', TfidfVectorizer()),
    ('clf', RandomForestClassifier(random_state=42))
    ])
    rfc_pipeline.fit(X_train,y_train)
    rfc_predicted = rfc_pipeline.predict(X_test)

    all_predictions = [{"name":"Random Forest","predictions":rfc_predicted}, {"name":"Support Vector Machine","predictions":sv_predicted}, {"name":"K-Nearest","predictions":knn_predicted},{"name":"Stochastic Gradient Descent","predictions":sgd_predicted}, {"name":"Naive Bayes", "predictions":nb_predicted}]

    for a in all_predictions:
        print(a["name"])
        calculate_metrics(y_test,a["predictions"])
        a["Accuracy"]= accuracy_score(y_test,a["predictions"])
        print()
    
    with open('sgd_model.pkl', 'wb') as files:
        pickle.dump(sgd_pipeline, files)
    with open('sv_model.pkl', 'wb') as files:
        pickle.dump(sv_pipeline, files)
    with open('knn_model.pkl', 'wb') as files:
        pickle.dump(knn_pipeline, files)

if __name__ == "__main__":
    main()
