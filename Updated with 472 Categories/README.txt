1. To check the category for a text, write these commands in command line. 
    - python main.py or python main.py {INPUT_PATH}
    If INPUT_PATH variable is not provided or is invalid, 
    the script automatically reads input.txt file present in the main.py directory

2. In the case of a wrong classification, the script asks for category code as per the encoding.json file. 
Please write "yes" when it classification was successful, and "no" when it was classified unsucessfully.
The top five categories are sorted in descending order by their probability.   


----------Files 

- There are 3 pickle files for the models that are trained

- Main.py is the driver which contains all the code for running the program and loads the trained model. The model is trained 
again when it gives a wrong prediction.

- Model.py file contains code for our model training, data processing and data cleaning