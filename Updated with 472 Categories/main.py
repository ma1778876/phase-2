import pickle
from pprint import pprint
import json

import sys
from csv import writer
import os
from nbformat import write
import numpy as np
from deep_translator import GoogleTranslator
translator = GoogleTranslator(source='de', target='en')


def read_input(input_path):
    with open(input_path,"r", encoding="utf-8") as f:
        contents = f.read()
        
    return contents


def fetch_top_5(probs_sv, encoding):
    best_n_sv = np.argsort(probs_sv, axis=1)[:,-5:]
    result = []
    with open('encoding.json') as json_file:
        encoding = json.load(json_file)
    for a in best_n_sv[:]:
        arr = [encoding[str(b)] for b in a]
        result = list(zip(range(1,len(arr[::-1])+1),arr[::-1], a[::-1]))
    return result


def preprocess_input(contents):
    content_list = []
    start = 0 
    while(start<len(contents)):
        a = contents[start:start+4999]
        content_list.append(a)
        start=start+4999
    input_text = ""
    for a in content_list:
        input_text = input_text + " " + (translator.translate(a)).replace("\n", " ") 
    return input_text

def writefb(category,input_text,link=""):
    if(len(input_text)>10):
        with open('data/structured-data/data_fb.csv', 'a+', newline='') as f_object:
            writer_object = writer(f_object)
            text = input_text.encode("utf-8")
            writer_object.writerow([category,link,text])

def main():
    input_path = "input.txt"
    if(len(sys.argv)>1):
        path = sys.argv[1]
        if os.path.exists(path):
            input_path = path
    filename = "sv_model.pkl"
    with open('encoding.json') as json_file:
        encoding = json.load(json_file)

    loaded_model = pickle.load(open(filename, 'rb'))
    contents = read_input(input_path)
    input_text = preprocess_input(contents)
    probs_sv = loaded_model.decision_function([input_text])
    pprint("Predictions:")
    pprint(fetch_top_5(probs_sv,encoding))
    prediction_confirmation = input("Do you think the predictions are correct (Yes/No)\n")
    if ((prediction_confirmation.lower())!="yes"):
        correct_category = input("What was the correct category? Please check encoding file and assign the relevant category code.")
        category_name = encoding[str(correct_category)] 
        writefb(category=category_name,input_text=input_text)
        print("Retraining model for future predictions ...")
        import model
        model.main()
    else:
        print("Yay!")


if __name__=="__main__":
    main()
